set nocompatible
call plug#begin('~/.vim/plugged')

Plug 'tpope/vim-rails'
Plug 'scrooloose/nerdtree'
Plug 'tpope/vim-endwise'
Plug 'kien/ctrlp.vim'
Plug 'tpope/vim-surround'
Plug 'altercation/vim-colors-solarized'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'rizzatti/dash.vim'
Plug 'airblade/vim-gitgutter'
Plug 'tomasr/molokai'
Plug 'reedes/vim-colors-pencil'
Plug 'elixir-lang/vim-elixir'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-fugitive'
Plug 'majutsushi/tagbar'
Plug 'xolox/vim-easytags'
Plug 'xolox/vim-misc'
Plug 'jiangmiao/auto-pairs'
Plug 'scrooloose/nerdcommenter'
Plug 'jeffkreeftmeijer/vim-numbertoggle'

call plug#end()

" set t_Co=256

syntax enable
let g:solarized_termcolors=16
set background=dark
colorscheme pencil
let g:airline_powerline_fonts = 1

:set encoding=utf-8

set autoindent
set tabstop=2
set expandtab
set shiftwidth=2
set nowrap

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop> 
noremap <Right> <Nop>
map <C-n> :NERDTreeToggle<CR>

:set number

autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

let g:ctrlp_dont_split = 'NERD'
nmap <Leader>kb :NERDTreeToggle<CR>

au BufRead,BufNewFile *.rabl setf ruby

set hidden

" =========== GREP via AG ==============

" The Silver Searcher
if executable('ag')
  " Use ag over grep
  set grepprg=ag\ --nogroup\ --nocolor

  " Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'

  " ag is fast enough that CtrlP doesn't need to cache
  let g:ctrlp_use_caching = 0
endif

nnoremap K :grep! "\b<C-R><C-W>\b"<CR>:cw<CR>

" =========== TAGBAR ==============

nmap <F8> :TagbarToggle<CR>

let g:tagbar_type_elixir = {
    \ 'ctagstype' : 'elixir',
    \ 'kinds' : [
        \ 'f:functions',
        \ 'functions:functions',
        \ 'c:callbacks',
        \ 'd:delegates',
        \ 'e:exceptions',
        \ 'i:implementations',
        \ 'a:macros',
        \ 'o:operators',
        \ 'm:modules',
        \ 'p:protocols',
        \ 'r:records'
    \ ]
\ }

let g:tagbar_type_ruby = {
    \ 'kinds' : [
        \ 'm:modules',
        \ 'c:classes',
        \ 'd:describes',
        \ 'C:contexts',
        \ 'f:methods',
        \ 'F:singleton methods'
    \ ]
\ }