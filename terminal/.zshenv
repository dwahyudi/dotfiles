# User configuration

export PATH=“$PATH:/Users/dwiwahyudi/.rvm/gems/ruby-2.1.5/bin:/Users/dwiwahyudi/.rvm/gems/ruby-2.1.5@global/bin:/Users/dwiwahyudi/.rvm/rubies/ruby-2.1.5/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/Applications/Meld.app/Contents/MacOS:/sbin:/Users/dwiwahyudi/.rvm/bin”
# export MANPATH="/usr/local/man:$MANPATH"

export NVM_DIR="/Users/dwiwahyudi/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting
export ANDROID_HOME=$HOME/Library/Android/sdk
export PATH=$ANDROID_HOME/tools:$PATH
export PATH=$ANDROID_HOME/platform-tools:$PATH
export PATH="$HOME/.rbenv/bin:$PATH"
export GOPATH=$HOME/Works/go
export PATH=$PATH:$GOPATH/bin
export ES_HOME=~/elasticsearch-1.7.6
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_131.jdk/Contents/Home
export PATH=$ES_HOME/bin:$JAVA_HOME/bin:$PATH
eval "$(rbenv init -)"
source ~/.exports